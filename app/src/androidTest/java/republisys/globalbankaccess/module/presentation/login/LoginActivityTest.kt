package republisys.globalbankaccess.module.presentation.login

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.closeSoftKeyboard
import android.support.test.espresso.action.ViewActions.typeText
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.mvp.moviedbapi.base.AbstractTest
import com.nhaarman.mockito_kotlin.mock
import org.hamcrest.core.Is.`is`
import org.junit.Assert.assertThat
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import republisys.globalbankaccess.R

/**
 * Created by republisys on 10/24/17.
 */

@RunWith(AndroidJUnit4::class)
@android.support.test.filters.LargeTest
class LoginActivityTest : AbstractTest() {

    @get:Rule
    val activityTestRule = ActivityTestRule<LoginActivity>(LoginActivity::class.java)


    @Test
    @Throws(Exception::class)
    fun ensureTextChangesWork() {

        onView(withId(R.id.username)).perform(typeText("Tayaggerald2509@gmail.com"), closeSoftKeyboard())
        onView(withId(R.id.password)).perform(typeText("Password1a"), closeSoftKeyboard())
        onView(withId(R.id.confirmpasword)).perform(typeText("Password1a"), closeSoftKeyboard())

        val isValidUsername = activityTestRule.activity.viewModel.usernameObservableValidator.isValid
        val isValidPassword = activityTestRule.activity.viewModel.passwordObservableValidator.isValid
        val isValidConfirmPassword = activityTestRule.activity.viewModel.confirmPasswordObservableValidator.isValid

        assertThat(isValidUsername, `is`<Boolean>(true))
        assertThat(isValidPassword, `is`<Boolean>(true))
        assertThat(isValidConfirmPassword, `is`<Boolean>(true))

    }

}