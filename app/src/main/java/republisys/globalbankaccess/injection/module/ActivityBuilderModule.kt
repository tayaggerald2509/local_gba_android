package republisys.globalbankaccess.injection.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import mcig.a420cloud.application.modules.PresenterFactoryModule
import republisys.globalbankaccess.module.presentation.login.LoginActivity
import republisys.globalbankaccess.module.presentation.main.MainActivity

/**
 * Created by republisys on 10/23/17.
 */
@Module(includes = arrayOf(PresenterFactoryModule::class))
internal abstract class ActivityBuilderModule {

    @ContributesAndroidInjector
    internal abstract fun loginActivity(): LoginActivity

    @ContributesAndroidInjector
    internal abstract fun mainActivity(): MainActivity

}