package mcig.a420cloud.application.modules

import dagger.Module
import dagger.Provides
import republisys.globalbankaccess.module.interactor.SystemSettingsInteractor
import republisys.globalbankaccess.module.interactor.UserEntityInteractor
import republisys.globalbankaccess.module.repository.SystemSettingsRepository
import republisys.globalbankaccess.module.repository.UserRepository

/**
 * Created by republisys on 9/27/17.
 */

@Module(includes = arrayOf(RepositoryBuilderModule::class))
class InteractorBuilderModule {

    @Provides
    fun userInteractorProvided(userRepository: UserRepository): UserEntityInteractor = UserEntityInteractor(userRepository)

    @Provides
    fun systemSettingsInteractorProvided(systemSettingsRepository: SystemSettingsRepository): SystemSettingsInteractor = SystemSettingsInteractor(systemSettingsRepository)

}