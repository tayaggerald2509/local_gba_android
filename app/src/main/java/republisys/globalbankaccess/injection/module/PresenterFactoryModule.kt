package mcig.a420cloud.application.modules

import dagger.Module
import dagger.Provides
import republisys.globalbankaccess.module.interactor.SystemSettingsInteractor
import republisys.globalbankaccess.module.interactor.UserEntityInteractor
import republisys.globalbankaccess.module.presentation.login.LoginPresenterFactory

/**
 * Created by republisys on 7/19/17.
 */

@Module(includes = arrayOf(RepositoryBuilderModule::class))
class PresenterFactoryModule{

    @Provides
    fun entryPresenterFactory(systemSettingsInteractor: SystemSettingsInteractor): LoginPresenterFactory = LoginPresenterFactory(systemSettingsInteractor)

}
