package republisys.globalbankaccess.module.data.entity.raw;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

import android.support.annotation.NonNull;

import mcig.base.domain.BaseRaw;
import republisys.globalbankaccess.module.data.entity.local.CountryEntity;

/**
 * Created by republisys on 10/25/17.
 */

@AutoValue
public abstract class CountryEntityRaw {

    public abstract String id();

    public abstract String name();

    public abstract String flag();

    public abstract String flag_rectangle();

    public abstract String currency();

    public abstract String customer_bps();

    public abstract String consumer_bps();

    public abstract String gross_margin();

    public abstract String country_all();

    @NonNull
    public static TypeAdapter<CountryEntityRaw> typeAdapter(@NonNull final Gson gson) {
        return new AutoValue_CountryEntityRaw.GsonTypeAdapter(gson);
    }

    @NonNull
    public static Builder builder() {
        return new AutoValue_CountryEntityRaw.Builder();
    }

    @AutoValue.Builder
    public interface Builder {

        Builder id(final String id);
        Builder name(final String name);
        Builder flag(final String flag);
        Builder flag_rectangle(final String flag_rectangle);
        Builder currency(final String currency);
        Builder customer_bps(final String costumer_bps);
        Builder consumer_bps(final String consumer_bps);
        Builder gross_margin(final String gross_margin);
        Builder country_all(final String country_all);

        CountryEntityRaw build();
    }
}

