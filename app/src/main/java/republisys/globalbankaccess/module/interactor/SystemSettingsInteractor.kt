package republisys.globalbankaccess.module.interactor

import android.support.annotation.NonNull
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import mcig.base.common.rx.UnwrapOptionTransformer
import mcig.base.domain.BaseInteractor
import mcig.base.domain.ReactiveInteractor
import polanski.option.Option
import republisys.globalbankaccess.module.data.entity.local.CountryEntity
import republisys.globalbankaccess.module.repository.SystemSettingsRepository
import republisys.globalbankaccess.module.repository.UserRepository
import javax.inject.Inject

/**
 * Created by republisys on 10/26/17.
 */
class SystemSettingsInteractor @Inject constructor(@NonNull val repository: SystemSettingsRepository) : BaseInteractor(),
        ReactiveInteractor.RetrieveInteractor<Void, List<CountryEntity>> {

    override fun getBehaviorStream(params: Option<Void>): Flowable<List<CountryEntity>> {
        return repository.allCountryDrafts.flatMapSingle { this.fetchWhenNoneAndThenDrafts(it) }
                .compose(UnwrapOptionTransformer.create())
    }

    private fun fetchWhenNoneAndThenDrafts(drafts: Option<List<CountryEntity>>): Single<Option<List<CountryEntity>>> {
        return fetchWhenNone(drafts).andThen(Single.just(drafts))
    }

    private fun fetchWhenNone(drafts: Option<List<CountryEntity>>): Completable {
        return if (drafts.isNone)
            repository.fetchCountryDrafts()
        else
            Completable.complete()
    }
}