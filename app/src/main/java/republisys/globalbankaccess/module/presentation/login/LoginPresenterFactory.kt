package republisys.globalbankaccess.module.presentation.login

import mcig.base.presentation.base.BasePresenterFactory
import republisys.globalbankaccess.module.interactor.SystemSettingsInteractor
import republisys.globalbankaccess.module.interactor.UserEntityInteractor
import republisys.globalbankaccess.module.presentation.login.LoginPresenter
import javax.inject.Inject

/**
 * Created by republisys on 9/25/17.
 */
class LoginPresenterFactory @Inject constructor(private val systemSettingsInteractor: SystemSettingsInteractor) : BasePresenterFactory<LoginPresenter> {

    override fun create(): LoginPresenter {
        return LoginPresenter(systemSettingsInteractor)
    }

}