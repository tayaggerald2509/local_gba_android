package republisys.globalbankaccess.module.presentation.login

import mcig.base.presentation.base.BaseView
import republisys.globalbankaccess.module.data.entity.local.CountryEntity

/**
 * Created by republisys on 9/25/17.
 */
interface LoginView: BaseView {

    fun showMainActivity()

}