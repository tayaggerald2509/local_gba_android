package republisys.globalbankaccess.module.presentation.main

import android.app.Activity
import mcig.base.presentation.base.BaseActivity
import republisys.globalbankaccess.R

/**
 * Created by republisys on 10/27/17.
 */
class MainActivity: BaseActivity(){

    override fun injectComponents(): Activity  = this

    override fun getLayoutId(): Int = R.layout.activity_main
}