package republisys.globalbankaccess.module.repository;

import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import mcig.base.data.store.ReactiveStore;
import polanski.option.Option;
import republisys.globalbankaccess.module.data.entity.local.CountryEntity;
import republisys.globalbankaccess.module.data.entity.mapper.CountryEntityMapper;
import republisys.globalbankaccess.module.data.entity.raw.CountryEntityRaw;
import republisys.globalbankaccess.module.data.entity.raw.ResultRaw;
import republisys.globalbankaccess.module.domain.RestService;

/**
 * Created by republisys on 10/26/17.
 */

@Singleton
public class SystemSettingsRepository {

    @NonNull
    private ReactiveStore<String, CountryEntity> store;

    @NonNull
    public RestService restService;

    @NonNull
    private CountryEntityMapper countryEntityMapper;

    @Inject
    public SystemSettingsRepository(@NonNull ReactiveStore<String, CountryEntity> store,
                             @NonNull RestService creditService,
                             @NonNull CountryEntityMapper creditDraftMapper) {
        this.store = store;
        this.restService = creditService;
        this.countryEntityMapper = creditDraftMapper;
    }

    @NonNull
    public Flowable<Option<List<CountryEntity>>> getAllCountryDrafts() {
        return store.getAll();
    }

    public Completable fetchCountryDrafts() {
        return restService.getCountries()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .flatMapObservable(it -> Observable.fromIterable(it.dataList()))
                .map(countryEntityMapper)
                .toList()
                .doOnSuccess(store::replaceAll)
                .toCompletable();
    }
}
