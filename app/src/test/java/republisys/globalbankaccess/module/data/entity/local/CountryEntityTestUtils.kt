package republisys.globalbankaccess.module.data.entity.local

import org.junit.Assert.*

/**
 * Created by republisys on 10/25/17.
 */
class CountryEntityTestUtils {


    companion object {

        fun countryEntityTestBuilder(): CountryEntity.Builder {
            return CountryEntity.builder()
                    .id("sdsdsd")
                    .country_all("1")
                    .consumer_bps("sddsds")
                    .currency("sdsdsd")
                    .flag("sdsdsd")
                    .flag_rectangle("sdsdsd")
                    .customer_bps("sdsdsd")
                    .gross_margin("sddsdsd")
                    .name("sddfs")
        }


    }

}