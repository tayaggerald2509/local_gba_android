package mcig.base.common.rx

import org.reactivestreams.Publisher

import io.reactivex.Flowable
import io.reactivex.FlowableTransformer
import io.reactivex.functions.Predicate
import polanski.option.Option
import polanski.option.OptionUnsafe

/**
 * Filters out all Option of NONE if any, but if Some, then unwraps and returns the value.
 */
class UnwrapOptionTransformer<T> : FlowableTransformer<Option<T>, T> {

    override fun apply(upstream: Flowable<Option<T>>): Publisher<T> {
        return upstream.filter({ it.isSome }).map({ OptionUnsafe.getUnsafe(it) })
    }

    companion object {

        fun <T> create(): UnwrapOptionTransformer<T> {
            return UnwrapOptionTransformer()
        }
    }
}
