package mcig.base.data.cache

import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.functions.Consumer
import io.reactivex.functions.Function
import io.reactivex.schedulers.Schedulers
import mcig.base.common.providers.TimestampProvider
import mcig.base.common.utils.ListUtils
import mcig.base.data.store.Store
import polanski.option.Option
import polanski.option.Option.none
import polanski.option.Option.ofObj
import java.util.concurrent.ConcurrentHashMap

/**
 * Generic memory cache with timeout for the entries.
 */
class Cache<Key, Value> : Store.MemoryStore<Key, Value> {

    private val timestampProvider: TimestampProvider

    private val extractKeyFromModel: Function<Value, Key>

    private val itemLifespanMs: Option<Long>

    private val cache = ConcurrentHashMap<Key, CacheEntry<Value>>()

    constructor(extractKeyFromModel: Function<Value, Key>,
                timestampProvider: TimestampProvider) : this(extractKeyFromModel, timestampProvider, none<Long>())

    constructor(extractKeyFromModel: Function<Value, Key>,
                timestampProvider: TimestampProvider,
                timeoutMs: Long) : this(extractKeyFromModel, timestampProvider, ofObj(timeoutMs))


    private constructor(extractKeyFromModel: Function<Value, Key>,
                        timestampProvider: TimestampProvider,
                        timeoutMs: Option<Long>) {
        this.timestampProvider = timestampProvider
        this.itemLifespanMs = timeoutMs
        this.extractKeyFromModel = extractKeyFromModel
    }

    override fun putSingular(value: Value) {
        Single.fromCallable<Key> { extractKeyFromModel.apply(value) }
                .subscribeOn(Schedulers.computation())
                .subscribe { key -> cache.put(key, createCacheEntry(value)) }
    }

    override fun putAll(values: List<Value>) {
        Observable.fromIterable(values)
                .toMap(extractKeyFromModel, Function<Value, CacheEntry<Value>> { this.createCacheEntry(it) })
                .subscribeOn(Schedulers.computation())
                .subscribe(Consumer<Map<Key, CacheEntry<Value>>> { cache.putAll(it) })
    }

    override fun getAll(): Maybe<List<Value>> {
        return Observable.fromIterable(cache.values)
                .filter { this.notExpired(it) }
                .map { it.cachedObject() }
                .toList().filter { ListUtils.isNotEmpty(it) }
                .subscribeOn(Schedulers.computation())
    }

    override fun getSingular(key: Key): Maybe<Value> {
        return Maybe.fromCallable<Any> { cache.contains(key) }
                .filter { it as Boolean }
                .map { _ -> cache[key]!! }
                .filter { this.notExpired(it) }
                .map { it.cachedObject() }
                .subscribeOn(Schedulers.computation())
    }

    override fun clear() {
        cache.clear()
    }

    private fun createCacheEntry(value: Value): CacheEntry<Value> {
        return CacheEntry.builder<Value>().cachedObject(value)
                .creationTimestamp(timestampProvider.currentTimeMillis())
                .build()
    }

    private fun notExpired(cacheEntry: CacheEntry<Value>): Boolean {
        return itemLifespanMs.match({ lifespanMs -> cacheEntry.creationTimestamp() + lifespanMs!! > timestampProvider.currentTimeMillis() }
                // When lifespan was not set the items in the cache never expire
        ) { true }
    }
}
