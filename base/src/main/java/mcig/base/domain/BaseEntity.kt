package mcig.base.domain

import com.google.gson.annotations.SerializedName
import com.remind101.auto.value.realm.AvPrimaryKey

abstract class BaseEntity {

    @AvPrimaryKey
    @SerializedName("id")
    abstract fun id(): String

}
