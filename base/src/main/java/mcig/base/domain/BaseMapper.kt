package mcig.base.domain

import io.reactivex.functions.Function

/**
 * Created by republisys on 9/25/17.
 */

abstract class BaseMapper<R, E> : Function<R, E> {

}
