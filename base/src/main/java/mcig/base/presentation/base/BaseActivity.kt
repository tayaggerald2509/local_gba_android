package mcig.base.presentation.base

import android.app.Activity
import android.graphics.Color
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.annotation.Nullable
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.afollestad.materialdialogs.GravityEnum
import com.afollestad.materialdialogs.MaterialDialog
import dagger.android.AndroidInjection
import estansaas.fonebayad.utils.DialogBuilder
import mcig.base.R

/**
 * Created by republisys on 7/26/17.
 */

abstract class BaseActivity : AppCompatActivity() {

    private var isDialogNetworkCalled = false
    private var isEnableBinding = false

    public override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(injectComponents())
        super.onCreate(savedInstanceState)
    }

    abstract fun injectComponents(): Activity
    @LayoutRes abstract fun getLayoutId(): Int

    fun onError(error: String) {
        Log.e("ERROR", error)
    }

    private fun dialog(init: DialogBuilder.() -> Unit): MaterialDialog {
        return DialogBuilder(this, init).build()
    }

    fun createDialog(title: String, message: String, positive: String, negative: String, onPositive: () -> Unit?, onNegative: () -> Unit?) {

        dialog {

            title(title)

            content(message)

            positive(positive)

            negative(negative)

            gravity(GravityEnum.START)

            onPositiveClick {
                onPositive.invoke()
            }

            onNegativeClick {
                onNegative.invoke()
            }
        }.show()
    }

    fun createDialog(title: String, message: String, neutral: String, onNeutral: () -> Unit?) {
        dialog {
            title(title)

            content(message)

            neutral(neutral)

            gravity(GravityEnum.END)

            onNeutralClick {
                onNeutral.invoke()
            }
        }.show()
    }

}
