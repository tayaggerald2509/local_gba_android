package mcig.base.presentation.base

import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.annotation.LayoutRes
import android.support.annotation.Nullable
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import mcig.base.R

/**
 * Created by republisys on 8/14/17.
 */

abstract class BaseFragment : Fragment() {

    @Nullable
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater!!.inflate(getLayoutId(), container, false)
    }

    @LayoutRes abstract fun getLayoutId(): Int

}
