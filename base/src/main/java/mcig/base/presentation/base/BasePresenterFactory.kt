package mcig.base.presentation.base

/**
 * Created by gerald.tayag on 2/9/2017.
 */

interface BasePresenterFactory<out P : BasePresenter<*>> {

    fun create(): P
}
