package mcig.base.util.validator

import android.databinding.BaseObservable
import android.text.TextUtils

/**
 * Created by republisys on 10/12/17.
 */
class ObservableValidatorField<T> : BaseObservable {

    var validator: BaseValidator<T>? = null
    var isValid: Boolean = false

    var errorMessage: String? = null
        set(errorMessage) {
            if (errorMessage == null) {
                hideErrorMessage()
            } else if (errorMessage != this.errorMessage) {
                field = errorMessage
                notifyChange()
            }
        }

    var value: T? = null
        set(value) {
            if (value != null && value != this.value) {
                field = value
                if (!validate()) {
                    notifyChange()
                }
            }
        }

    constructor(value: T, validator: BaseValidator<T>, validate: Boolean = false) {
        this.value = value
        this.validator = validator

        if (validate) {
            validate()
        }
    }

    fun validate(): Boolean {

        if (validator != null) {
            isValid = validator!!.isValid(value!!)
            errorMessage = if (isValid) "" else validator!!.getErrorMessage()

            notifyChange()
            return true
        }
        return false
    }

    fun hideErrorMessage() {
        if (errorMessage != null && !TextUtils.isEmpty(errorMessage)) {
            errorMessage = ""
            notifyChange()
        }
    }

    fun showError(errorMessage: String?){
        if (errorMessage != null && !TextUtils.isEmpty(errorMessage)) {
            this.errorMessage = errorMessage
            notifyChange()
        }
    }
}