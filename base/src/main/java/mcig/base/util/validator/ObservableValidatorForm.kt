package mcig.base.util.validator

import android.databinding.Observable
import android.databinding.ObservableBoolean

/**
 * Created by republisys on 10/12/17.
 */
class ObservableValidatorForm(vararg var fields : ObservableValidatorField<*>): ObservableBoolean() {

    init {
        for (field in fields){
            field.addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
                override fun onPropertyChanged(observable: Observable, i: Int) {
                    set(areAllFieldsValid())
                }
            })
        }
    }

    private fun areAllFieldsValid(): Boolean {
        var isValid = fields.isNotEmpty()
        for (field in fields) {
            isValid = isValid and field.isValid
            if (!isValid) {
                break
            }
        }
        return isValid
    }

}