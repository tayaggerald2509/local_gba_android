package mcig.base.util.validator.rules

/**
 * Created by republisys on 10/12/17.
 */
class RegexRule(val regex: String, error: String) : Validator<String>(error) {

    override fun isValid(t: String?): Boolean {
        return t != null && t.matches(regex.toRegex())
    }

}