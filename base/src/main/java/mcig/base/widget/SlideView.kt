package mcig.base.widget

import android.content.Context
import android.graphics.Paint
import android.graphics.RectF
import android.graphics.drawable.AnimatedVectorDrawable
import android.support.graphics.drawable.VectorDrawableCompat
import android.transition.Slide
import android.util.AttributeSet
import android.view.View

import mcig.base.R

/**
 * Created by republisys on 11/7/17.
 */

class SlideView(context: Context, attrs: AttributeSet?, defStyle: Int) : View(context, attrs, defStyle) {

    constructor(context: Context) : this(context, null, R.styleable.SlideViewTheme_slideViewStyle)
    constructor(context: Context, attrs: AttributeSet) : this(context, attrs, R.styleable.SlideViewTheme_slideViewStyle)

    private var mSliderHeightDp: Float = 72F
    private var mSliderWidthDp: Float = 280F
    private var mSliderHeight: Int = 0
    private var mSliderWidth: Int = 0

    private var mAreaHeight: Int = 0
    private var mAreaWidth: Int = 0

    private var mActualAreaWidth: Int = 0
    private var mBorderRadius: Int = -1

    private var mActualAreaMargin: Int = 0
    private var mOriginAreaMargin: Int = 0

    private lateinit var mTextMessage: String

    private var mTextSize: Int = 0

    private var mTextYPosition = -1f
    private var mTextXPosition = -1f

    private var mOuterColor: Int = 0
    private var mInnerColor: Int = 0

    private var mPosition: Int = 0
        set(value) {
            field = value
            if (mAreaWidth - mAreaHeight == 0) {
                mPositionPerc = 0f
                mPositionPercInv = 1f
                return
            }

            mPositionPerc = value.toFloat().div(mAreaWidth.minus(mAreaHeight))
            mPositionPercInv = 1.minus(value).div(mAreaWidth.minus(mAreaHeight)).toFloat()
        }

    private var mPositionPerc: Float = 0f
    private var mPositionPercInv: Float = 1f

    private var mIconMargin: Int = 0
    private var mArrowMargin: Int = 0
    private var mArrowAngle: Float = 0f
    private var mTickMargin: Int = 0

    private lateinit var mDrawableArrow: VectorDrawableCompat

    private lateinit var mDrawableTick: AnimatedVectorDrawable
    private var mFlagDrawTick: Boolean = false

    private val mInnerPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val mOuterPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)

    private val mTextPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)

    private lateinit var mInnerRect: RectF
    private lateinit var mOuterRect: RectF

    private val mGraceValue: Float = 0.8F

    private var mLastX: Float = 0F

    private var mIsCompleted: Boolean = false

    private var mOutlineProviders: List<Any> = arrayListOf()

    private var isLocked: Boolean = false

    private lateinit var onSlideViewAnimationLister: OnSlideViewAnimationEventListener
    private lateinit var onSlideComplete: OnSlideCompleteListener
    private lateinit var onSlideReset: OnSlideResetListener

    init {

        //val layoutAttrs = context.theme.obtainStyledAttributes(attrs, R.styleable.SlideView, defStyle, )

    }


}


interface OnSlideViewAnimationEventListener {


    fun onSlideAnimationComplete(view: SlideView, threshold: Float)

    fun onSlideAnimationEnded(view: SlideView)

    fun onSlideAnimationReset(view: SlideView)

    fun onSlideAnimationResetEnded(view: SlideView)

}


interface OnSlideCompleteListener {
    fun onSlideComplete(view: SlideView)
}

interface OnSlideResetListener {
    fun onSlideReset(view: SlideView)
}